"use strict";

require('dotenv').config();

module.exports = {
  "sequelize-pg-generator": {
    database: {
      host: process.env.DB_HOST,
      port: 5432,
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      schema: ["public", "seguridad", "persona", "general"]
    },
    template: {
      folder: './tools/template',
    },
    output: {
      folder: "./src/infrastructure/model",
    },
    generate: {
      "stripFirstTableFromHasMany": false,
      "addTableNameToManyToMany": false,
      "addRelationNameToManyToMany": false,
      "stripFirstTableNameFromManyToMany": false,
      "hasManyThrough": false,
      "belongsToMany": false,
      "skipTable": [] // Poner el nombre de las tablas de tratamiento especial separadas por una coma y crear los modelos dentro de 'definition-files-custom'
    }
  },
};
