'use strict';

// Definiendo asociaciones de las tablas
module.exports = function associations(models) {
	const {
		usuario_persona,
		usuario,
		usuario_rol,
		rol,
		rol_recurso,
		recurso,
		sistema,
		estado,
		persona,
		v_usuario_rol
	} = models;

	// Asociaciones tabla recurso
	recurso.belongsTo(estado, { foreignKey: { name: 'id_estado' }, as: 'estado' });
	estado.hasMany(recurso, { foreignKey: { name: 'id_estado' } });

	recurso.belongsTo(recurso, { foreignKey: { name: 'id_padre' }, as: 'padre' });
	recurso.hasMany(recurso, { foreignKey: { name: 'id_padre' } });

	// Asociaciones tabla rol
	rol.belongsTo(estado, { foreignKey: { name: 'id_estado' }, as: 'estado' });
	estado.hasMany(rol, { foreignKey: { name: 'id_estado' } });

	rol.belongsTo(sistema, { foreignKey: { name: 'id_sistema' }, as: 'sistema' });
	sistema.hasMany(rol, { foreignKey: { name: 'id_sistema' } });

	// Asociaciones tabla rol_recurso Muchos a muchos
	rol.belongsToMany(recurso, { foreignKey: { name: 'id_rol' }, through: rol_recurso, as: 'recursos' });
	recurso.belongsToMany(rol, { foreignKey: { name: 'id_recurso' }, through: rol_recurso, as: 'roles' });

	rol_recurso.belongsTo(estado, { foreignKey: { name: 'id_estado' }, as: 'estado' });
	estado.hasMany(rol_recurso, { foreignKey: { name: 'id_estado' } });

	// Asociaciones tabla sistema
	sistema.belongsTo(estado, { foreignKey: { name: 'id_estado' }, as: 'estado' });
	estado.hasMany(sistema, { foreignKey: { name: 'id_estado' } });

	// Asociaciones tabla usuario
	usuario.belongsTo(estado, { foreignKey: { name: 'id_estado' }, as: 'estado' });
	estado.hasMany(usuario, { foreignKey: { name: 'id_estado' } });

	// Asociaciones tabla usuario_rol  m:m
	usuario.belongsToMany(rol, { foreignKey: { name: 'id_usuario' }, through: usuario_rol, as: 'roles' });
	rol.belongsToMany(usuario, { foreignKey: { name: 'id_rol' }, through: usuario_rol, as: 'usuarios' });
	usuario_rol.belongsTo(rol, { foreignKey: { name: 'id_rol' }, as: 'roles' });
	rol.hasMany(usuario_rol, { foreignKey: { name: 'id_rol' } });
	usuario_rol.belongsTo(usuario, { foreignKey: { name: 'id_usuario' }, as: 'usuarios' });
	usuario.hasMany(usuario_rol, { foreignKey: { name: 'id_usuario' } });

	usuario.belongsToMany(persona, { foreignKey: { name: 'id_usuario' }, through: usuario_persona, as: 'personas' });
	persona.belongsToMany(usuario, { foreignKey: { name: 'id_persona' }, through: usuario_persona, as: 'usuarios' });
	usuario_persona.belongsTo(persona, { foreignKey: { name: 'id_persona' }, as: 'personas' });
	persona.hasMany(usuario_persona, { foreignKey: { name: 'id_persona' } });
	usuario_persona.belongsTo(usuario, { foreignKey: { name: 'id_usuario' }, as: 'usuarios' });
	usuario.hasMany(usuario_persona, { foreignKey: { name: 'id_usuario' } });

	usuario_rol.belongsTo(estado, { foreignKey: { name: 'id_estado' }, as: 'estado' });
	estado.hasMany(usuario_rol, { foreignKey: { name: 'id_estado' } });

	// Asociaciones tabla usuario_persona
	usuario_persona.belongsTo(estado, { foreignKey: { name: 'id_estado' }, as: 'estado' });
	estado.hasMany(usuario_persona, { foreignKey: { name: 'id_estado' } });

	// Asociaciones tabla v_usuario_rol
	v_usuario_rol.belongsTo(persona, { foreignKey: { name: 'id_persona' }, as: 'persona' });
	persona.hasMany(v_usuario_rol, { foreignKey: { name: 'id_persona' } });

	v_usuario_rol.belongsTo(usuario, { foreignKey: { name: 'id_usuario' }, as: 'usuario' });
	usuario.hasMany(v_usuario_rol, { foreignKey: { name: 'id_usuario' } });

	v_usuario_rol.belongsTo(recurso, { foreignKey: { name: 'id_recurso' }, as: 'recurso' });
	recurso.hasMany(v_usuario_rol, { foreignKey: { name: 'id_recurso' } });

	v_usuario_rol.belongsTo(recurso, { foreignKey: { name: 'id_padre' }, as: 'padre' });
	recurso.hasMany(v_usuario_rol, { foreignKey: { name: 'id_padre' } });

	return models;
};