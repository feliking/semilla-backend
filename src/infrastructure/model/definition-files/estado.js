/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    descripcion: {
      type: DataTypes.STRING(200),
      allowNull: true,
      xlabel: lang.t("fields.descripcion")
    },
    abrev: {
      type: DataTypes.STRING(10),
      allowNull: true,
      xlabel: lang.t("fields.abrev")
    },
    tipo: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.tipo")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let estado = sequelize.define("estado", fields, {
    timestamps: false,
    tableName: "estado",
    schema: "general",
  });
  // estado.removeAttribute('id') // Descomentar para las vistas
  return estado;
};