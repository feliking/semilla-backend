/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    id_estado: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_estado")
    },
    id_persona: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_persona")
    },
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_usuario")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let usuarioPersona = sequelize.define("usuario_persona", fields, {
    timestamps: false,
    tableName: "usuario_persona",
    schema: "seguridad",
  });
  // usuarioPersona.removeAttribute('id') // Descomentar para las vistas
  return usuarioPersona;
};