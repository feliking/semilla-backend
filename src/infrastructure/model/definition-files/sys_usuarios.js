/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    usuario: {
      type: DataTypes.STRING(50),
      allowNull: false,
      xlabel: lang.t("fields.usuario")
    },
    contrasena: {
      type: DataTypes.STRING(255),
      allowNull: true,
      xlabel: lang.t("fields.contrasena")
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.email")
    },
    email_verified_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields.email_verified_at")
    },
    cargo: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.cargo")
    },
    ultimo_login: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields.ultimo_login")
    },
    nro_intentos: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.nro_intentos")
    },
    fecha_bloqueo: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields.fecha_bloqueo")
    },
    fecha_reseteo_pass: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields.fecha_reseteo_pass")
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: true,
      xlabel: lang.t("fields.token")
    },
    extra: {
      type: DataTypes.JSON,
      allowNull: true,
      xlabel: lang.t("fields.extra")
    },
    estado: {
      type: DataTypes.ENUM('ACTIVO', 'INACTIVO', 'PENDIENTE', 'BLOQUEADO'),
      allowNull: false,
      xlabel: lang.t("fields.estado")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
    id_entidad: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_entidad")
    },
    id_rol: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_rol")
    },
    id_persona: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_persona")
    },
  }
  let sysUsuarios = sequelize.define("sys_usuarios", fields, {
    timestamps: false,
    tableName: "sys_usuarios",
    schema: "public",
  });
  // sysUsuarios.removeAttribute('id') // Descomentar para las vistas
  return sysUsuarios;
};