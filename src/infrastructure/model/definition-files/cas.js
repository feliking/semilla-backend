/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    fecha_tramite: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      xlabel: lang.t("fields.fecha_tramite")
    },
    anios: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.anios")
    },
    id_estado: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_estado")
    },
    id_persona: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_persona")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
    id_vacacion_escala: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_vacacion_escala")
    },
  }
  let cas = sequelize.define("cas", fields, {
    timestamps: false,
    tableName: "cas",
    schema: "persona",
  });
  // cas.removeAttribute('id') // Descomentar para las vistas
  return cas;
};