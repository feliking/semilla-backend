/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      xlabel: lang.t("fields.name")
    },
  }
  let sequelizeSeeders = sequelize.define("sequelize_seeders", fields, {
    timestamps: false,
    tableName: "sequelize_seeders",
    schema: "public",
  });
  // sequelizeSeeders.removeAttribute('id') // Descomentar para las vistas
  return sequelizeSeeders;
};