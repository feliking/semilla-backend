/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    state: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t("fields.state")
    },
    parametros: {
      type: DataTypes.JSONB,
      allowNull: true,
      xlabel: lang.t("fields.parametros")
    },
    tokens: {
      type: DataTypes.JSONB,
      allowNull: true,
      xlabel: lang.t("fields.tokens")
    },
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_usuario")
    },
    estado: {
      type: DataTypes.ENUM('INICIO', 'ACTIVO', 'ELIMINADO'),
      allowNull: false,
      xlabel: lang.t("fields.estado")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let sysAuth = sequelize.define("sys_auth", fields, {
    timestamps: false,
    tableName: "sys_auth",
    schema: "public",
  });
  // sysAuth.removeAttribute('id') // Descomentar para las vistas
  return sysAuth;
};