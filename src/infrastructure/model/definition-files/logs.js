/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nivel: {
      type: DataTypes.ENUM('ERROR', 'INFO', 'ADVERTENCIA'),
      allowNull: false,
      xlabel: lang.t("fields.nivel")
    },
    tipo: {
      type: DataTypes.STRING(50),
      allowNull: true,
      xlabel: lang.t("fields.tipo")
    },
    mensaje: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t("fields.mensaje")
    },
    referencia: {
      type: DataTypes.TEXT,
      allowNull: true,
      xlabel: lang.t("fields.referencia")
    },
    ip: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.ip")
    },
    fecha: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields.fecha")
    },
    usuario: {
      type: DataTypes.STRING(255),
      allowNull: true,
      xlabel: lang.t("fields.usuario")
    },
  }
  let logs = sequelize.define("logs", fields, {
    timestamps: false,
    tableName: "logs",
    schema: "public",
  });
  // logs.removeAttribute('id') // Descomentar para las vistas
  return logs;
};