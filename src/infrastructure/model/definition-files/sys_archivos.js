/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    filename: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t("fields.filename")
    },
    nombre: {
      type: DataTypes.STRING(255),
      allowNull: true,
      xlabel: lang.t("fields.nombre")
    },
    mimetype: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.mimetype")
    },
    path: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t("fields.path")
    },
    size: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      xlabel: lang.t("fields.size")
    },
    es_imagen: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      xlabel: lang.t("fields.es_imagen")
    },
    img_width: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.img_width")
    },
    img_height: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.img_height")
    },
    estado: {
      type: DataTypes.ENUM('ACTIVO', 'INACTIVO', 'ELIMINADO'),
      allowNull: false,
      xlabel: lang.t("fields.estado")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let sysArchivos = sequelize.define("sys_archivos", fields, {
    timestamps: false,
    tableName: "sys_archivos",
    schema: "public",
  });
  // sysArchivos.removeAttribute('id') // Descomentar para las vistas
  return sysArchivos;
};