/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false,
      xlabel: lang.t("fields.nombre")
    },
    descripcion: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t("fields.descripcion")
    },
    sigla: {
      type: DataTypes.STRING(20),
      allowNull: true,
      xlabel: lang.t("fields.sigla")
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.email")
    },
    telefonos: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.telefonos")
    },
    direccion: {
      type: DataTypes.TEXT,
      allowNull: true,
      xlabel: lang.t("fields.direccion")
    },
    web: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.web")
    },
    info: {
      type: DataTypes.JSON,
      allowNull: true,
      xlabel: lang.t("fields.info")
    },
    subdomain: {
      type: DataTypes.STRING(30),
      allowNull: true,
      xlabel: lang.t("fields.subdomain")
    },
    codigo_portal: {
      type: DataTypes.STRING(20),
      allowNull: true,
      xlabel: lang.t("fields.codigo_portal")
    },
    nit: {
      type: DataTypes.STRING(20),
      allowNull: true,
      xlabel: lang.t("fields.nit")
    },
    usuario: {
      type: DataTypes.STRING(50),
      allowNull: true,
      xlabel: lang.t("fields.usuario")
    },
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_usuario")
    },
    estado: {
      type: DataTypes.ENUM('ACTIVO', 'INACTIVO'),
      allowNull: false,
      xlabel: lang.t("fields.estado")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let sysEntidades = sequelize.define("sys_entidades", fields, {
    timestamps: false,
    tableName: "sys_entidades",
    schema: "public",
  });
  // sysEntidades.removeAttribute('id') // Descomentar para las vistas
  return sysEntidades;
};