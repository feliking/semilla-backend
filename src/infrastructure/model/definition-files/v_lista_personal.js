/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    id_persona: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_persona")
    },
    nro_doc: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nro_doc")
    },
    nro_complemento: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nro_complemento")
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nombre")
    },
    item: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.item")
    },
    fecha_ingreso: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.fecha_ingreso")
    },
    cargo: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.cargo")
    },
    puesto: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.puesto")
    },
    id_estado: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_estado")
    },
    estado: {
      type: DataTypes.STRING(200),
      allowNull: true,
      xlabel: lang.t("fields.estado")
    },
    id_unidad: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_unidad")
    },
    unidad: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.unidad")
    },
    id_cargo: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_cargo")
    },
    id_puesto: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_puesto")
    },
    id_item: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_item")
    },
    id_movilidad: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_movilidad")
    },
  }
  let vListaPersonal = sequelize.define("v_lista_personal", fields, {
    timestamps: false,
    tableName: "v_lista_personal",
    schema: "persona",
  });
  // vListaPersonal.removeAttribute('id') // Descomentar para las vistas
  return vListaPersonal;
};