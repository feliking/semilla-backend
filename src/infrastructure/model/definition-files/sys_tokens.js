/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    token: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t("fields.token")
    },
    tipo: {
      type: DataTypes.ENUM('USUARIO', 'OPERADOR', 'ENTIDAD'),
      allowNull: false,
      xlabel: lang.t("fields.tipo")
    },
    estado: {
      type: DataTypes.ENUM('ACTIVO', 'INACTIVO'),
      allowNull: false,
      xlabel: lang.t("fields.estado")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_usuario")
    },
    id_entidad: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_entidad")
    },
  }
  let sysTokens = sequelize.define("sys_tokens", fields, {
    timestamps: false,
    tableName: "sys_tokens",
    schema: "public",
  });
  // sysTokens.removeAttribute('id') // Descomentar para las vistas
  return sysTokens;
};