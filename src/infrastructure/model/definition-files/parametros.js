/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t("fields.nombre")
    },
    valor: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t("fields.valor")
    },
    label: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t("fields.label")
    },
    descripcion: {
      type: DataTypes.TEXT,
      allowNull: true,
      xlabel: lang.t("fields.descripcion")
    },
    _user_created: {
      type: DataTypes.STRING(255),
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.STRING(255),
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let parametros = sequelize.define("parametros", fields, {
    timestamps: false,
    tableName: "parametros",
    schema: "public",
  });
  // parametros.removeAttribute('id') // Descomentar para las vistas
  return parametros;
};