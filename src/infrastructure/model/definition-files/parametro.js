/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    valor: {
      type: DataTypes.STRING(200),
      allowNull: true,
      xlabel: lang.t("fields.valor")
    },
    tipo: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.tipo")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
    estado: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.estado")
    },
  }
  let parametro = sequelize.define("parametro", fields, {
    timestamps: false,
    tableName: "parametro",
    schema: "general",
  });
  // parametro.removeAttribute('id') // Descomentar para las vistas
  return parametro;
};