/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.descripcion")
    },
    id_estado: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_estado")
    },
    nivel: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.nivel")
    },
    id_padre: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_padre")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
    icono: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.icono")
    },
    ruta: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.ruta")
    },
  }
  let recurso = sequelize.define("recurso", fields, {
    timestamps: false,
    tableName: "recurso",
    schema: "seguridad",
  });
  // recurso.removeAttribute('id') // Descomentar para las vistas
  return recurso;
};