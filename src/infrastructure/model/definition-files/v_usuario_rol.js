/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    id_persona: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_persona")
    },
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_usuario")
    },
    nombres: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nombres")
    },
    nro_doc: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nro_doc")
    },
    nombre_usuario: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nombre_usuario")
    },
    correo: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.correo")
    },
    rol: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.rol")
    },
    sistema: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.sistema")
    },
    sigla: {
      type: DataTypes.STRING(20),
      allowNull: true,
      xlabel: lang.t("fields.sigla")
    },
    id_recurso: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_recurso")
    },
    id_padre: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_padre")
    },
    cargo: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.cargo")
    },
    puesto: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.puesto")
    },
    nro_item: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nro_item")
    },
    nombre_unidad: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nombre_unidad")
    },
  }
  let vUsuarioRol = sequelize.define("v_usuario_rol", fields, {
    timestamps: false,
    tableName: "v_usuario_rol",
    schema: "seguridad",
  });
  // vUsuarioRol.removeAttribute('id') // Descomentar para las vistas
  return vUsuarioRol;
};