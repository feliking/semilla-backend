/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    ap_paterno: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.ap_paterno")
    },
    ap_materno: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.ap_materno")
    },
    ap_casada: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.ap_casada")
    },
    nombres: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.nombres")
    },
    id_tipo_documento: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_tipo_documento")
    },
    nro_doc: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.nro_doc")
    },
    nro_complemento: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nro_complemento")
    },
    pais_nacimiento: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.pais_nacimiento")
    },
    departamento_nacimiento: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.departamento_nacimiento")
    },
    nacionalidad: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nacionalidad")
    },
    ciudad_residencia: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.ciudad_residencia")
    },
    sexo: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.sexo")
    },
    direccion: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.direccion")
    },
    telefono_domicilio: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.telefono_domicilio")
    },
    telefono_alternativo: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.telefono_alternativo")
    },
    celular: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.celular")
    },
    nro_licencia_conducir: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nro_licencia_conducir")
    },
    grupo_sanguineo: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.grupo_sanguineo")
    },
    foto: {
      type: DataTypes.STRING(200),
      allowNull: true,
      xlabel: lang.t("fields.foto")
    },
    afp: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.afp")
    },
    nro_nua: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nro_nua")
    },
    banco: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.banco")
    },
    nro_cuenta_bancaria: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nro_cuenta_bancaria")
    },
    libreta_serv_militar: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.libreta_serv_militar")
    },
    nro_registro: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.nro_registro")
    },
    anio_serv_militar: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.anio_serv_militar")
    },
    id_estado: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_estado")
    },
    fecha_nacimiento: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      xlabel: lang.t("fields.fecha_nacimiento")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
    expedido: {
      type: DataTypes.STRING(10),
      allowNull: true,
      xlabel: lang.t("fields.expedido")
    },
  }
  let persona = sequelize.define("persona", fields, {
    timestamps: false,
    tableName: "persona",
    schema: "persona",
  });
  // persona.removeAttribute('id') // Descomentar para las vistas
  return persona;
};