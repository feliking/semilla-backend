/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    codigo: {
      type: DataTypes.STRING(20),
      allowNull: false,
      xlabel: lang.t("fields.codigo")
    },
    metodo: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t("fields.metodo")
    },
    descripcion: {
      type: DataTypes.TEXT,
      allowNull: true,
      xlabel: lang.t("fields.descripcion")
    },
    entidad: {
      type: DataTypes.STRING(255),
      allowNull: true,
      xlabel: lang.t("fields.entidad")
    },
    url: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t("fields.url")
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t("fields.token")
    },
    tipo: {
      type: DataTypes.ENUM('PUBLICO', 'CONVENIO'),
      allowNull: false,
      xlabel: lang.t("fields.tipo")
    },
    estado: {
      type: DataTypes.ENUM('ACTIVO', 'INACTIVO'),
      allowNull: false,
      xlabel: lang.t("fields.estado")
    },
    _user_created: {
      type: DataTypes.STRING(255),
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.STRING(255),
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let serviciosIop = sequelize.define("servicios_iop", fields, {
    timestamps: false,
    tableName: "servicios_iop",
    schema: "public",
  });
  // serviciosIop.removeAttribute('id') // Descomentar para las vistas
  return serviciosIop;
};