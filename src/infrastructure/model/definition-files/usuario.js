/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    ci: {
      type: DataTypes.STRING,
      allowNull: true,
      xlabel: lang.t("fields.ci")
    },
    nombre_usuario: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.nombre_usuario")
    },
    correo: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.correo")
    },
    id_estado: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_estado")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
    ultimo_login: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields.ultimo_login")
    },
  }
  let usuario = sequelize.define("usuario", fields, {
    timestamps: false,
    tableName: "usuario",
    schema: "seguridad",
  });
  // usuario.removeAttribute('id') // Descomentar para las vistas
  return usuario;
};