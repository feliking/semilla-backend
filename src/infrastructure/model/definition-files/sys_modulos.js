/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    ruta: {
      type: DataTypes.STRING(50),
      allowNull: true,
      xlabel: lang.t("fields.ruta")
    },
    label: {
      type: DataTypes.STRING(50),
      allowNull: true,
      xlabel: lang.t("fields.label")
    },
    icono: {
      type: DataTypes.STRING(30),
      allowNull: true,
      xlabel: lang.t("fields.icono")
    },
    orden: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.orden")
    },
    estado: {
      type: DataTypes.ENUM('ACTIVO', 'INACTIVO'),
      allowNull: false,
      xlabel: lang.t("fields.estado")
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t("fields.visible")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
    id_modulo: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_modulo")
    },
    id_seccion: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.id_seccion")
    },
  }
  let sysModulos = sequelize.define("sys_modulos", fields, {
    timestamps: false,
    tableName: "sys_modulos",
    schema: "public",
  });
  // sysModulos.removeAttribute('id') // Descomentar para las vistas
  return sysModulos;
};