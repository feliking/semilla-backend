/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombres: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t("fields.nombres")
    },
    primer_apellido: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.primer_apellido")
    },
    segundo_apellido: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.segundo_apellido")
    },
    nombre_completo: {
      type: DataTypes.STRING(255),
      allowNull: true,
      xlabel: lang.t("fields.nombre_completo")
    },
    tipo_documento: {
      type: DataTypes.ENUM('CI', 'PASAPORTE', 'OTRO'),
      allowNull: false,
      xlabel: lang.t("fields.tipo_documento")
    },
    tipo_documento_otro: {
      type: DataTypes.STRING(50),
      allowNull: true,
      xlabel: lang.t("fields.tipo_documento_otro")
    },
    nro_documento: {
      type: DataTypes.STRING(50),
      allowNull: true,
      xlabel: lang.t("fields.nro_documento")
    },
    fecha_nacimiento: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      xlabel: lang.t("fields.fecha_nacimiento")
    },
    telefono: {
      type: DataTypes.STRING(50),
      allowNull: true,
      xlabel: lang.t("fields.telefono")
    },
    movil: {
      type: DataTypes.STRING(50),
      allowNull: true,
      xlabel: lang.t("fields.movil")
    },
    nacionalidad: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.nacionalidad")
    },
    pais_nacimiento: {
      type: DataTypes.STRING(100),
      allowNull: true,
      xlabel: lang.t("fields.pais_nacimiento")
    },
    genero: {
      type: DataTypes.ENUM('M', 'F', 'OTRO'),
      allowNull: true,
      xlabel: lang.t("fields.genero")
    },
    observacion: {
      type: DataTypes.TEXT,
      allowNull: true,
      xlabel: lang.t("fields.observacion")
    },
    estado: {
      type: DataTypes.ENUM('ACTIVO', 'INACTIVO'),
      allowNull: false,
      xlabel: lang.t("fields.estado")
    },
    estado_verificacion: {
      type: DataTypes.ENUM('VERIFICADO_SEGIP', 'OBSERVADO_SEGIP', 'NO_EXISTE_SEGIP', 'POR_VERIFICAR', 'VERIFICADO'),
      allowNull: true,
      xlabel: lang.t("fields.estado_verificacion")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let sysPersonas = sequelize.define("sys_personas", fields, {
    timestamps: false,
    tableName: "sys_personas",
    schema: "public",
  });
  // sysPersonas.removeAttribute('id') // Descomentar para las vistas
  return sysPersonas;
};