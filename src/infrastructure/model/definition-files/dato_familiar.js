/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre_completo: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.nombre_completo")
    },
    id_parentesco: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_parentesco")
    },
    nacionalidad: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.nacionalidad")
    },
    fecha_nacimiento: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      xlabel: lang.t("fields.fecha_nacimiento")
    },
    id_persona: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_persona")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let datoFamiliar = sequelize.define("dato_familiar", fields, {
    timestamps: false,
    tableName: "dato_familiar",
    schema: "persona",
  });
  // datoFamiliar.removeAttribute('id') // Descomentar para las vistas
  return datoFamiliar;
};