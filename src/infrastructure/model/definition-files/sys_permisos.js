/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    create: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t("fields.create")
    },
    read: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t("fields.read")
    },
    update: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t("fields.update")
    },
    delete: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t("fields.delete")
    },
    firma: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t("fields.firma")
    },
    csv: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t("fields.csv")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
    id_rol: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_rol")
    },
    id_modulo: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_modulo")
    },
  }
  let sysPermisos = sequelize.define("sys_permisos", fields, {
    timestamps: false,
    tableName: "sys_permisos",
    schema: "public",
  });
  // sysPermisos.removeAttribute('id') // Descomentar para las vistas
  return sysPermisos;
};