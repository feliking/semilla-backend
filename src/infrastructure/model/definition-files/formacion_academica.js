/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    colegio: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.colegio")
    },
    nivel_vencido: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.nivel_vencido")
    },
    diploma_bachiller: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.diploma_bachiller")
    },
    nro_diploma: {
      type: DataTypes.STRING,
      allowNull: false,
      xlabel: lang.t("fields.nro_diploma")
    },
    id_persona: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields.id_persona")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let formacionAcademica = sequelize.define("formacion_academica", fields, {
    timestamps: false,
    tableName: "formacion_academica",
    schema: "persona",
  });
  // formacionAcademica.removeAttribute('id') // Descomentar para las vistas
  return formacionAcademica;
};