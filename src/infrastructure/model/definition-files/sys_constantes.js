/*------------------------------------------------------------------------------------

ARCHIVOS MODELOS GENERADOS AUTOMATICAMENTE, SE RECOMIENDA NO MODIFICAR LOS ARCHIVOS

Author: Felix Antonio Mamani Quispe
Email: felixddxd@gmail.com
La Paz - Bolivia

------------------------------------------------------------------------------------*/
"use strict";
const lang = require("../../lang");
const util = require("../../lib/util");
module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    grupo: {
      type: DataTypes.STRING(50),
      allowNull: false,
      xlabel: lang.t("fields.grupo")
    },
    codigo: {
      type: DataTypes.STRING(20),
      allowNull: false,
      xlabel: lang.t("fields.codigo")
    },
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false,
      xlabel: lang.t("fields.nombre")
    },
    descripcion: {
      type: DataTypes.TEXT,
      allowNull: true,
      xlabel: lang.t("fields.descripcion")
    },
    orden: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields.orden")
    },
    estado: {
      type: DataTypes.ENUM('ACTIVO', 'INACTIVO'),
      allowNull: false,
      xlabel: lang.t("fields.estado")
    },
    _user_created: {
      type: DataTypes.INTEGER,
      allowNull: false,
      xlabel: lang.t("fields._user_created")
    },
    _user_updated: {
      type: DataTypes.INTEGER,
      allowNull: true,
      xlabel: lang.t("fields._user_updated")
    },
    _created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      xlabel: lang.t("fields._created_at")
    },
    _updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
      xlabel: lang.t("fields._updated_at")
    },
  }
  let sysConstantes = sequelize.define("sys_constantes", fields, {
    timestamps: false,
    tableName: "sys_constantes",
    schema: "public",
  });
  // sysConstantes.removeAttribute('id') // Descomentar para las vistas
  return sysConstantes;
};