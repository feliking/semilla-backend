'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function estadosRepository (models, Sequelize) {
  const { estado } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    return estado.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, estado),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, estado, t),
    deleteItem: (id, t) => Repository.deleteItem(id, estado, t)
  };
};
