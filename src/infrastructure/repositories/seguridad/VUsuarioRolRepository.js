"use strict";

const { getQuery } = require("../../lib/util");
const Repository = require("../Repository");

module.exports = function VUsuarioRolRepository(models, Sequelize) {
  const { v_usuario_rol, persona, usuario, recurso } = models;
  const Op = Sequelize.Op;

  function findAll(params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        model: persona,
        as: "persona",
      },
      {
        model: usuario,
        as: "usuario",
      },
      {
        model: recurso,
        as: "recurso",
      },
			{
        model: recurso,
        as: "padre",
      },
    ];

    if (params.id_persona) {
      query.where.id_persona = params.id_persona;
    }
    if (params.id_usuario) {
      query.where.id_usuario = params.id_usuario;
    }

    if (params.id_recurso) {
      query.where.id_recurso = params.id_recurso;
    }

    if (params.id_padre) {
      query.where.id_padre = params.id_padre;
    }

		if (params.nombre_usuario) {
      query.where.nombre_usuario = params.nombre_usuario;
    }

    return v_usuario_rol.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, v_usuario_rol),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, v_usuario_rol, t),
    deleteItem: (id, t) => Repository.deleteItem(id, v_usuario_rol, t),
  };
};
