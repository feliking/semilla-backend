'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function rol_recursoRepository(models, Sequelize) {
    const { rol_recurso } = models;
    const Op = Sequelize.Op;

    function findAll(params = {}) {
        let query = getQuery(params);
        query.where = {};

        return rol_recurso.findAndCountAll(query);
    }

    return {
        findAll,
        findById: (id) => Repository.findById(id, rol_recurso),
        createOrUpdate: (item, t) => Repository.createOrUpdate(item, rol_recurso, t),
        deleteItem: (id, t) => Repository.deleteItem(id, rol_recurso, t)
    };
};