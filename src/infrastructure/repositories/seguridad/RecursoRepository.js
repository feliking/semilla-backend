"use strict";

const { getQuery } = require("../../lib/util");
const Repository = require("../Repository");

module.exports = function recursoRepository(models, Sequelize) {
  const { recurso, rol, rol_recurso } = models;
  const Op = Sequelize.Op;

  function findAll(params = {}) {
    let query = getQuery(params);
    query.where = {
      id_estado: {
        [Op.or]: [1, 20]
      }
    };

    query.include = [
      {
        model: rol,
        as: "roles",
        where: {
          id_sistema: params.id_sistema
        }
      },
      {
        model: recurso,
        as: 'padre'
      }
    ];

    return recurso.findAndCountAll(query);
  }

  async function createOrUpdate (object, roles, t) {
    const cond = {
      where: {
        id: object.id || null
      }
    };

    let rol;
    roles.forEach (item => {
      if (item.descripcion.toLowerCase() === 'administrador') {
        rol = item
      }
    })

    if (rol.id) {
      const item = await recurso.findOne(cond);
  
      if (item) {
        let updated;
        try {
          if (t) {
            cond.transaction = t;
          }
          updated = await recurso.update(object, cond);
        } catch (e) {
          if (t) {
            await t.rollback();
          }
          console.log(e);
        }
    
        const result = updated ? await recurso.findOne(cond) : item;
    
        if (result) {
          return result.toJSON();
        }
        return null;
      }
    
      let result;
      try {
        result = await recurso.create(object, t ? { transaction: t } : {});
        const id = result.dataValues.id
        await rol_recurso.create({
          id_recurso: id,
          id_rol: rol.id,
          id_estado: 1,
          _created_at: new Date(),
          _user_created: result.dataValues._user_created
        })

      } catch (e) {
        if (t) {
          await t.rollback();
        }
        console.log(e);
      }
    
      return result.toJSON();
    }

    return null
  
    
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, recurso),
    createOrUpdate,
    deleteItem: (id, t) => Repository.deleteItem(id, recurso, t),
  };
};
