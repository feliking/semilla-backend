"use strict";

const { getQuery } = require("../../lib/util");
const Repository = require("../Repository");

module.exports = function usuarioRepository(models, Sequelize) {
  const { usuario, estado, rol, sistema, recurso, persona, usuario_rol } = models;
  const Op = Sequelize.Op;

  function findAll(params = {}) {
    let query = getQuery(params);
    query.where = {
      id_estado: {
        [Op.or]: [1, 20]
      }
    };
    query.include = [
			{
				model: estado,
				as: 'estado'
			},
			{
				model: rol,
				as: 'roles',
        include: [
          {
            model: sistema,
            as: 'sistema'
          },
          {
            model: recurso,
            as: 'recursos'
          }
        ]
			},
      {
        model: persona,
        as: 'personas',
        through: {
          where: {
            id_estado: 1
          }
        }
      },
		];

    if (params.nombre_usuario) {
      query.where.nombre_usuario = {
        [Op.iLike]: `%${params.nombre_usuario}%`
      };
    }

    if (params.correo) {
      query.where.correo = {
        [Op.iLike]: `%${params.correo}%`
      };
    }

    return usuario.findAndCountAll(query);
  }

  async function findById (id) {
    const result = await usuario.findByPk(id, {
      include: [
        {
          model: rol,
          as: 'roles'
        },
        {
          model: persona,
          as: 'personas'
        }
      ]
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }
  // find user
  async function findByUsername (nombre_usuario, include = true) {
    let cond = {
      where: {
        nombre_usuario
      }
    };

    if (include) {
      cond.include = [
        {
          model: rol,
          as: 'roles'
        },
        {
          model: persona,
          as: 'persona'
        }
      ];
    } else {
      cond.include = [
        {
          model: rol,
          as: 'roles'
        }
      ];
    }

    const result = await usuario.findOne(cond);

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  // find userByCi
  async function findByCi (persona) {
    let cond = {
      attributes: ['id', 'usuario'],
      include: [{
        attributes: ['id'],
        model: personas,
        as: 'persona',
        where: persona,
        require: true
      }]
    };

    const result = await usuario.findOne(cond);
    if (result) {
      return result.toJSON();
    }
    return null;
  }

  async function createOrUpdate(user, t) {
    const cond = {
      where: {
        id: user.id || null,
      }
    };
  
    const item = await usuario.findOne(cond);
  
    if (item) {
      let updated;
      let deleted_relations;
      try {
        updated = await usuario.update(user, cond);
        if (user.roles) {
          deleted_relations = await usuario_rol.destroy({
            where: {
              id_usuario: item.id
            }
          });
          await user.roles.forEach(elem => {
            usuario_rol.create({
              id_estado: 1,
              id_usuario: item.id,
              id_rol: elem.id,
              _user_created: user._user_updated,
              _created_at: new Date()
            })
          });
        }
      } catch (e) {
        console.log(e);
      }
      const result = updated ? await usuario.findOne(cond) : item;
  
      if (result) {
        return result.toJSON();
      }
      return null;
    }
  
    let result;
    try {
      result = await usuario.create(user, t ? { transaction: t } : {});
    } catch (e) {
      errorHandler(e);
    }
    return result.toJSON();
  }

  async function getUserByUsername (nombre_usuario, id_sistema) {
    const cond = {
      where: {
        nombre_usuario
      },
      include: [
        {
          model: rol,
          as: 'roles',
          include: [
            {
              model: sistema,
              as: 'sistema'
            },
            {
              model: recurso,
              as: 'recursos',
              where: {
                id_estado: 1
              }
            }
          ],
          where: {
            id_sistema,
            id_estado: 1
          }
        },
        {
          model: persona,
          as: 'personas',
          through: {
            where: {
              id_estado: 1
            }
          }
        }
      ]
    };
    let result = await usuario.findOne(cond);
    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById,
    findByUsername,
    findByCi,
    createOrUpdate,
    deleteItem: (id, t) => Repository.deleteItem(id, usuario, t),
    getUserByUsername,
    runQuery: (consulta) => Repository.runQuery(consulta)
  };
};
