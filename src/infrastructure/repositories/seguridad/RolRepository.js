"use strict";

const { result } = require("validate.js");
const { getQuery } = require("../../lib/util");
const Repository = require("../Repository");

module.exports = function rolRepository(models, Sequelize) {
  const { rol, sistema, usuario, recurso, rol_recurso } = models;
  const Op = Sequelize.Op;

  function findAll(params = {}) {
    let query = getQuery(params);
    query.where = {
      id_sistema: params.id_sistema,
      id_estado: {
        [Op.or]: [1, 20]
      }
    };

    query.include = [
			{
				model: sistema,
				as: 'sistema'
			},
      {
        model: recurso,
        as: 'recursos'
      }
		];

    return rol.findAndCountAll(query);
  }

  async function agregarRecursos (params = {}, t) {
    let cond = {
      where: {
        id_rol: params.recursos.id
      }
    };
    try {
      if (t) {
        cond.transaction = t;
      }
      await rol_recurso.destroy(cond)
      await params.recursos.recursos.forEach(elem => {
        rol_recurso.create({
          id_rol: params.recursos.id,
          id_recurso: elem.id,
          id_estado: 1,
          _created_at: params._created_at,
          _user_created: params._user_created
        });
      });
    } catch (error) {
      if (t) {
        t.rollback()
      }
      console.log(error)
    }
    return true;
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, rol),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, rol, t),
    deleteItem: (id, t) => Repository.deleteItem(id, rol, t),
    agregarRecursos
  };
};
