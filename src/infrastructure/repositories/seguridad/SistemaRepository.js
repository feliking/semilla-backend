"use strict";

const { getQuery } = require("../../lib/util");
const Repository = require("../Repository");

module.exports = function sistemaRepository(models, Sequelize) {
  const { sistema } = models;
  const Op = Sequelize.Op;

  function findAll(params = {}) {
    let query = getQuery(params);
    query.where = {};

    return sistema.findAndCountAll(query);
  }

  async function findBySigla(sigla) {
    const result = await sistema.findOne({
      where: {
        sigla
      },
    });
    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, sistema),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, sistema, t),
    deleteItem: (id, t) => Repository.deleteItem(id, sistema, t),
		findBySigla
  };
};
