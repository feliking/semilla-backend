'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function usuario_personaRepository(models, Sequelize) {
    const { usuario_persona } = models;
    const Op = Sequelize.Op;

    function findAll(params = {}) {
        let query = getQuery(params);
        query.where = {};

        return usuario_persona.findAndCountAll(query);
    }

    return {
        findAll,
        findById: (id) => Repository.findById(id, usuario_persona),
        createOrUpdate: (item, t) => Repository.createOrUpdate(item, usuario_persona, t),
        deleteItem: (id, t) => Repository.deleteItem(id, usuario_persona, t)
    };
};