'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function personaRepository (models, Sequelize) {
  const { persona, tipo_documento, estado } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        model: estado,
        as: 'estado'
      },
      {
        model: tipo_documento,
        as: 'tipo_documento'
      }
    ];

    return persona.findAndCountAll(query);
  }

  async function findById (id) {
    const result = await persona.findByPk(id, {
      include: [
        {
          model: tipo_documento,
          as: 'tipo_documento'
        }
      ]
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, persona, t),
    deleteItem: (id, t) => Repository.deleteItem(id, persona, t)
  };
};
