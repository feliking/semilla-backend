'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function formacionAcademicaRepository (models, Sequelize) {
  const { formacion_academica } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    return formacion_academica.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, formacion_academica),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, formacion_academica, t),
    deleteItem: (id, t) => Repository.deleteItem(id, formacion_academica, t)
  };
};
