'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function datosFamiliaresRepository (models, Sequelize) {
  const { datos_familiares } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    return datos_familiares.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, datos_familiares),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, datos_familiares, t),
    deleteItem: (id, t) => Repository.deleteItem(id, datos_familiares, t)
  };
};
