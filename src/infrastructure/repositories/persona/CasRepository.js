'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function casRepository (models, Sequelize) {
  const { cas } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    return cas.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, cas),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, cas, t),
    deleteItem: (id, t) => Repository.deleteItem(id, cas, t)
  };
};
