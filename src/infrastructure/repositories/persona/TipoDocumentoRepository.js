'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function tipoDocumentosRepository (models, Sequelize) {
  const { tipo_documento, estado } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};
    query.include = [
      {
        attributes: ['id','descripcion', 'abrev'],
        model: estado,
        as: 'estado'
      }
    ];

    return tipo_documento.findAndCountAll(query);
  }
  async function findById (id) {
    const result = await tipo_documento.findByPk(id, {
      include: [
        {
          attributes: ['id', 'descripcion', 'abrev'],
          model: estado,
          as: 'estado'
        }
      ]
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, tipo_documento, t),
    deleteItem: (id, t) => Repository.deleteItem(id, tipo_documento, t)
  };
};
