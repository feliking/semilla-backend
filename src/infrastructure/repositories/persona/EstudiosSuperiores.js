'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function estudiosSuperioresRepository (models, Sequelize) {
  const { estudios_superiores } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    return estudios_superiores.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, estudios_superiores),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, estudios_superiores, t),
    deleteItem: (id, t) => Repository.deleteItem(id, estudios_superiores, t)
  };
};
