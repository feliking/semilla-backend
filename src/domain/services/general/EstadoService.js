'use strict';

const debug = require('debug')('app:service:estado');
const Service = require('../Service');

module.exports = function estadoService (repositories, valueObjects, res) {
  const { EstadoRepository } = repositories;
  const {
    EstadoDescripcion,
    EstadoAbrev
  } = valueObjects;

  async function findAll (params = {}, rol, idEstado) {
    debug('Lista de estado|filtros');

    switch (rol) {
      case 'ADMIN':
        params.id_estado = idEstado;
        break;
      case 'USUARIO':
        params.id_estado = idEstado;
        break;
    }

    return Service.findAll(params, EstadoRepository, res, 'Estado');
  }

  async function findById (id) {
    debug('Buscando estado por ID', id);

    return Service.findById(id, EstadoRepository, res, 'Estado');
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar estado');

    validate(data);

    return Service.createOrUpdate(data, EstadoRepository, res, 'Estado');
  }

  async function deleteItem (id) {
    debug('Eliminando estado');

    return Service.deleteItem(id, EstadoRepository, res, 'Estado');
  }

  function validate (data) {
    Service.validate(data, {
      descripcion: EstadoDescripcion,
      abrev: EstadoAbrev
    });
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
