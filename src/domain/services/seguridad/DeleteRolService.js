'use strict';

const debug = require('debug')('app:service:rol');
const Service = require('../Service');

module.exports = function rolService(repositories, valueObjects, res) {
    const { RolRepository } = repositories;
    const {
        RolDescripcion,
        RolNivel,
        RolEstado,
        RolPersona

    } = valueObjects;

    async function findAll(params = {}, rol, idRol) {
        debug('Lista de Rol|filtros');

        switch (rol) {
            case 'ADMIN':
                params.id_asistencia = idRol;
                break;
            case 'USUARIO':
                params.id_asistencia = idRol;
                break;
        }

        return Service.findAll(params, RolRepository, res, 'Rol');
    }

    async function findById(id) {
        debug('Buscando rol por ID', id);

        return Service.findById(id, RolRepository, res, 'Rol');
    }

    async function createOrUpdate(data) {
        debug('Crear o actualizar rol');

        //validate(data);

        return Service.createOrUpdate(data, RolRepository, res, 'Rol');
    }

    async function deleteItem(id) {
        debug('Eliminando rol');

        return Service.deleteItem(id, RolRepository, res, 'Rol');
    }

    function validate(data) {
        Service.validate(data, {

            descricpcion: RolDescripcion,
            estado: RolEstado,
            persona: RolPersona,
            NavigationPreloadManager: RolNivel
        });
    }

    return {
        findAll,
        findById,
        createOrUpdate,
        deleteItem
    };
};