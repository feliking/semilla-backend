"use strict";

const debug = require("debug")("app:service:v_usuario_rol");
const Service = require("../Service");

module.exports = function VUsuarioRolService(repositories, valueObjects, res) {
  const { VUsuarioRolRepository } = repositories;

  async function findAll(params = {}, rol, idVUsuarioRol) {
    debug("Lista de entidad|filtros");

    return Service.findAll(params, VUsuarioRolRepository, res, "VUsuarioRol");
  }

  async function findById(id) {
    debug("Buscando entidad por ID", id);

    return Service.findById(id, VUsuarioRolRepository, res, "VUsuarioRol");
  }

  async function createOrUpdate(data) {
    debug("Crear o actualizar VPendientePorJefe");

    return Service.createOrUpdate(
      data,
      VUsuarioRolRepository,
      res,
      "VUsuarioRol"
    );
  }

  async function deleteItem(id) {
    debug("Eliminando entidad");

    return Service.deleteItem(id, VUsuarioRolRepository, res, "VUsuarioRol");
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
  };
};
