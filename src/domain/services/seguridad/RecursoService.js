"use strict";

const debug = require("debug")("app:service:recurso");
const Service = require("../Service");

module.exports = function recursoService(repositories, valueObjects, res) {
  const { RecursoRepository } = repositories;
  const { RecursoDescripcion, RecursoNivel, RecursoEstado, RecursoPersona } =
    valueObjects;

  async function findAll(params = {}, rol, idRecurso) {
    debug("Lista de Recurso|filtros");

    return Service.findAll(params, RecursoRepository, res, "Recurso");
  }

  async function findById(id) {
    debug("Buscando recurso por ID", id);

    return Service.findById(id, RecursoRepository, res, "Recurso");
  }

  async function createOrUpdate(data, roles) {
    debug("Crear o actualizar recurso");

    let item;
    try {
      item = await RecursoRepository.createOrUpdate(data, roles);
    } catch (e) {
      return res.error(e);
    }

    if (!item) {
      return res.warning(new Error(`El Recurso no pudo ser creado`));
    }

    return res.success(item);

    return Service.createOrUpdate(data, RecursoRepository, res, "Recurso");
  }

  async function deleteItem(id) {
    debug("Eliminando recurso");

    return Service.deleteItem(id, RecursoRepository, res, "Recurso");
  }

  function validate(data) {
    Service.validate(data, {
      descricpcion: RecursoDescripcion,
      estado: RecursoEstado,
      persona: RecursoPersona,
      NavigationPreloadManager: RecursoNivel,
    });
  }

  async function getMenu(roles) {
    let menu = [];
    let padres = [];
    let hijos = [];
    let recurso = {};
    let children = [];
    debug(`Obteniendo menú`);
    // Separando padres e hijos para clasificarlos por nivel
    roles.forEach((item) => {
      item.recursos.forEach((elem) => {
        if (elem.id_padre !== null) {
          hijos.push(elem);
        } else {
          padres.push(elem);
        }
      });
    });
    // Eliminando items repetidos del menú
    let aux = [];
    let find = false
    hijos.forEach(hijo => {
      aux.forEach(elem => {
        if (hijo.id == elem.id) {
          find = true;
        }
      });
      if (!find) {
        aux.push(hijo);
        find = false;
      }
    });
    hijos = aux;
    aux = [];
    find = false
    padres.forEach(padre => {
      aux.forEach(elem => {
        if (padre.id == elem.id) {
          find = true;
        }
      });
      if (!find) {
        aux.push(padre);
        find = false;
      }
    });
    padres = aux;
    // Creando items del menu insertando hijos donde sus padres
    padres.forEach((item) => {
      recurso = item;
      hijos.forEach((elem) => {
        if (recurso.id === elem.id_padre) {
          children.push(elem);
        }
      });
      recurso.children = children;
      recurso.children.sort((a, b) => {
        return a.nivel - b.nivel;
      });
      children = [];
      menu.push(recurso);
    });
    // Verificando si son padres y no tienen hijos para habilitarles el link de navegación
    menu.forEach((item) => {
      if (item.children.length > 0) {
        item.active = false;
      } else {
        item.active = true;
      }
    });
    return res.success(menu);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    getMenu,
  };
};
