'use strict';

const debug = require('debug')('app:service:usuario');
const moment = require('moment');
const crypto = require('crypto');
const { authenticate } = require('ldap-authentication')
const { text, mail } = require('../../../common');
const { generateToken } = require('../../../application/lib/auth');
const Service = require('../Service');

module.exports = function userService (repositories, valueObjects, res) {
  const RecursoService = require('./RecursoService')(repositories, valueObjects, res);

  const { transaction, Iop, UsuarioRepository, PersonaRepository, SistemaRepository, Parametro, Log } = repositories;
  const {
    UsuarioUsuario,
    UsuarioContrasena,
    UsuarioEmail,
    UsuarioCargo,
    UsuarioEstado,
    PersonaNombres,
    PersonaPrimerApellido,
    PersonaSegundoApellido,
    PersonaNombreCompleto,
    PersonaTipoDocumento,
    PersonaTipoDocumentoOtro,
    PersonaNroDocumento,
    PersonaFechaNacimiento,
    PersonaMovil,
    PersonaNacionalidad,
    PersonaPaisNacimiento,
    PersonaGenero,
    PersonaTelefono,
    PersonaEstado
  } = valueObjects;

  async function findAll (params = {}, rol, idEntidad) {
    debug('Lista de usuarios|filtros');

    return Service.findAll(params, UsuarioRepository, res, 'Usuarios');
  }

  async function findById (id) {
    debug('Buscando usuario por ID');

    return Service.findById(id, UsuarioRepository, res, 'Usuario');
  }

  async function createOrUpdate (data, rol = null, idEntidad = null) {
    debug('Crear o actualizar usuario', data);

    return Service.createOrUpdate(data, UsuarioRepository, res, 'Usuario');
  }

  async function update (data) {
    debug('Actualizar usuario');

    if (!data.id) {
      return res.error(new Error(`Se necesita el ID del usuario para actualizar el registro`));
    }

    let user;
    try {
      validateUser(data);
      user = await UsuarioRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.warning(new Error(`El usuario no pudo ser actualizado`));
    }

    return res.success(user);
  }

  async function deleteItem (id) {
    debug('Eliminando usuario');

    return Service.deleteItem(id, UsuarioRepository, res, 'Usuario');
  }

  async function userLdapExist (usuario, contrasena, opcionesLdap) {
    debug('Comprobando usuario en LDAP');
    opcionesLdap.username = usuario;
    opcionesLdap.userPassword = contrasena;
    try {
      let userLdap = await authenticate(opcionesLdap);
      if (userLdap) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
      // return res.error(error);
    }
  }

  async function userExist (usuario, sigla) {
    debug(`Comprobando si el usuario ${usuario} existe`);

    let result;
    let sistema;
    try {
      sistema = await SistemaRepository.findBySigla(sigla)
      debug(`Sistema encontrado: ${sistema}`);
      result = await UsuarioRepository.getUserByUsername(usuario, sistema.id)
      if (result == null) {
        return res.error(new Error(`Usuario o contraseña incorrectos o no tiene roles asignados en el sistema, consulte con el administrador`));
      }
      return res.success(result);
    } catch (e) {

      return res.error(e);
    }
  }

  async function getUser (usuario, include = true) {
    debug('Buscando usuario por nombre de usuario');

    let user;
    try {
      user = await UsuarioRepository.findByUsername(usuario, include);
    } catch (e) {
      return res.error(e);
    }

    if (!user) {
      return res.warning(new Error(`Usuario ${usuario} not found`));
    }

    return res.success(user);
  }

  async function getResponse (user, info = {}) {
    let respuesta;
    try {
      const usuario = user.nombre_usuario;
      // Actualizando el último login
      const now = moment().format('YYYY-MM-DD HH:mm:ss');
      await update({
        id: user.id,
        ultimo_login: now
      });
      let text = '';
      if (info.location) {
        text += `Location: ${info.location.country} -- ${info.location.city} <br />`;
      }
      if (info.navigator) {
        text += `Navigator: ${info.navigator}`;
      }
      Log.info(`El usuario: ${usuario} ingresó al sistema a las ${now}`, 'LOGIN', text, usuario, info.ip);

      // Obteniendo menu
      let menu = await RecursoService.getMenu(user.roles);
      // let permissions = menu.data.permissions;
      menu = menu.data;

      // Generando token
      let token = await generateToken(Parametro, usuario);

      // Formateando permisos
      // let permisos = {};
      // permissions.map(item => (permisos[item] = true));

      respuesta = {
        menu,
        token,
        // permisos,
        usuario: {
          'id': user.id,
          'id_persona': user.personas[0].id,
          'usuario': user.nombre_usuario,
          'nombres': user.personas[0].nombres,
          'ap_paterno': user.personas[0].ap_paterno,
          'ap_materno': user.personas[0].ap_materno,
          'email': user.correo,
          'roles': user.roles,
          'lang': 'es'
        },
        redirect: '/'
      };
      return respuesta;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  async function regenerar (id, idUsuario) {
    debug('Regenerar contraseña');
    try {
      let datos = await UsuarioRepository.findById(id);

      if (!datos && !datos.id) {
        return res.warning(new Error('El usuario no esta registrado'));
      }
      if (!datos.email) {
        return res.warning(new Error('El usuario no cuenta con un email registrado'));
      }
      const contrasena = crypto.randomBytes(4).toString('hex');
      const data = {
        id,
        contrasena,
        _user_updated: idUsuario
      };
      await UsuarioRepository.createOrUpdate(data);

      let correo = await mail.enviar({
        para: [datos.email],
        asunto: '<br> Nueva contraseña - APP',
        contenido: `Nueva contraseña: <strong>${contrasena}</strong>`
      });
      return res.success(correo);
    } catch (e) {
      return res.error(e);
    }
  }

  function validatePerson (data) {
    Service.validate(data, {
      nombres: PersonaNombres,
      primer_apellido: PersonaPrimerApellido,
      segundo_apellido: PersonaSegundoApellido,
      nombre_completo: PersonaNombreCompleto,
      tipo_documento: PersonaTipoDocumento,
      tipo_documento_otro: PersonaTipoDocumentoOtro,
      nro_documento: PersonaNroDocumento,
      fecha_nacimiento: PersonaFechaNacimiento,
      movil: PersonaMovil,
      nacionalidad: PersonaNacionalidad,
      pais_nacimiento: PersonaPaisNacimiento,
      genero: PersonaGenero,
      telefono: PersonaTelefono
    });
  }

  function validateUser (data) {
    Service.validate(data, {
      usuario: UsuarioUsuario,
      contrasena: UsuarioContrasena,
      email: UsuarioEmail,
      cargo: UsuarioCargo,
      estado: UsuarioEstado
    });
  }

  async function runQuery(data) {
    debug("Ejecutando consulta SQL");

    return Service.runQuery(data, UsuarioRepository, res);
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    userLdapExist,
    userExist,
    getUser,
    update,
    getResponse,
    regenerar,
    runQuery
  };
};