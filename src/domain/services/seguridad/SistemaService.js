"use strict";

const debug = require("debug")("app:service:sistema");
const Service = require("../Service");

module.exports = function sistemaService(repositories, valueObjects, res) {
  const { SistemaRepository } = repositories;
  const { SistemaDescripcion, SistemaNivel, SistemaEstado, SistemaPersona } =
    valueObjects;

  async function findAll(params = {}, rol, idSistema) {
    debug("Lista de Sistema|filtros");

    return Service.findAll(params, SistemaRepository, res, "Sistema");
  }

  async function findById(id) {
    debug("Buscando sistema por ID", id);

    return Service.findById(id, SistemaRepository, res, "Sistema");
  }

  async function createOrUpdate(data) {
    debug("Crear o actualizar sistema");

    //validate(data);

    return Service.createOrUpdate(data, SistemaRepository, res, "Sistema");
  }

  async function deleteItem(id) {
    debug("Eliminando sistema");

    return Service.deleteItem(id, SistemaRepository, res, "Sistema");
  }

  async function findBySigla(sigla) {
    debug("Buscando sistema por sigla");

    let item;
    try {
      item = await SistemaRepository.findBySigla(sigla);
    } catch (e) {
      return res.error(e);
    }

    if (!item) {
      return res.warning(new Error(`${sigla} not found`));
    }

    return res.success(item);
  }

  function validate(data) {
    Service.validate(data, {
      descricpcion: SistemaDescripcion,
      estado: SistemaEstado,
      persona: SistemaPersona,
      NavigationPreloadManager: SistemaNivel,
    });
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
		findBySigla
  };
};
