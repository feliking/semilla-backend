'use strict';

const debug = require('debug')('app:service:usuario_persona');
const Service = require('../Service');

module.exports = function usuario_personaService(repositories, valueObjects, res) {
    const { UsuarioPersonaRepository } = repositories;
    const {
        UsuarioPersonaDescripcion,
        UsuarioPersonaNivel,
        UsuarioPersonaEstado,
        UsuarioPersonaPersona

    } = valueObjects;

    async function findAll(params = {}, rol, idUsuarioPersona) {
        debug('Lista de UsuarioPersona|filtros');

        switch (rol) {
            case 'ADMIN':
                params.id_asistencia = idUsuarioPersona;
                break;
            case 'USUARIO':
                params.id_asistencia = idUsuarioPersona;
                break;
        }

        return Service.findAll(params, UsuarioPersonaRepository, res, 'UsuarioPersona');
    }

    async function findById(id) {
        debug('Buscando usuario_persona por ID', id);

        return Service.findById(id, UsuarioPersonaRepository, res, 'UsuarioPersona');
    }

    async function createOrUpdate(data) {
        debug('Crear o actualizar usuario_persona');

        //validate(data);

        return Service.createOrUpdate(data, UsuarioPersonaRepository, res, 'UsuarioPersona');
    }

    async function deleteItem(id) {
        debug('Eliminando usuario_persona');

        return Service.deleteItem(id, UsuarioPersonaRepository, res, 'UsuarioPersona');
    }

    function validate(data) {
        Service.validate(data, {

            descricpcion: UsuarioPersonaDescripcion,
            estado: UsuarioPersonaEstado,
            persona: UsuarioPersonaPersona,
            NavigationPreloadManager: UsuarioPersonaNivel
        });
    }

    return {
        findAll,
        findById,
        createOrUpdate,
        deleteItem
    };
};