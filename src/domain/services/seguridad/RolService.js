"use strict";

const debug = require("debug")("app:service:rol");
const Service = require("../Service");

module.exports = function rolService(
  repositories,
  valueObjects,
  res
) {
  const { RolRepository } = repositories;
  const { RolDescripcion, RolNivel, RolEstado, RolPersona } = valueObjects;

  async function findAll(params = {}, seguridad_rol, idRol) {
    debug("Lista de Rol|filtros");

    return Service.findAll(params, RolRepository, res, "Rol");
  }

  async function findById(id) {
    debug("Buscando seguridad_rol por ID", id);

    return Service.findById(id, RolRepository, res, "Rol");
  }

  async function createOrUpdate(data) {
    debug("Crear o actualizar seguridad_rol");

    //validate(data);

    return Service.createOrUpdate(data, RolRepository, res, "Rol");
  }

  async function deleteItem(id) {
    debug("Eliminando seguridad_rol");

    return Service.deleteItem(id, RolRepository, res, "Rol");
  }

  async function agregarRecursos(params) {
    debug("Agregando recursos");

    let item;
    try {
      item = await RolRepository.agregarRecursos(params);
    } catch (e) {
      return res.error(e);
    }

    return res.success(item);
  }

  function validate(data) {
    Service.validate(data, {
      descricpcion: RolDescripcion,
      estado: RolEstado,
      persona: RolPersona,
      NavigationPreloadManager: RolNivel,
    });
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    agregarRecursos
  };
};
