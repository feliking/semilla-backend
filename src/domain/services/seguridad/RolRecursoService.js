'use strict';

const debug = require('debug')('app:service:rol_recurso');
const Service = require('../Service');

module.exports = function rol_recursoService(repositories, valueObjects, res) {
    const { RolRecursoRepository } = repositories;
    const {
        RolRecursoDescripcion,
        RolRecursoNivel,
        RolRecursoEstado,
        RolRecursoPersona

    } = valueObjects;

    async function findAll(params = {}, rol, idRolRecurso) {
        debug('Lista de RolRecurso|filtros');

        switch (rol) {
            case 'ADMIN':
                params.id_asistencia = idRolRecurso;
                break;
            case 'USUARIO':
                params.id_asistencia = idRolRecurso;
                break;
        }

        return Service.findAll(params, RolRecursoRepository, res, 'RolRecurso');
    }

    async function findById(id) {
        debug('Buscando rol_recurso por ID', id);

        return Service.findById(id, RolRecursoRepository, res, 'RolRecurso');
    }

    async function createOrUpdate(data) {
        debug('Crear o actualizar rol_recurso');

        //validate(data);

        return Service.createOrUpdate(data, RolRecursoRepository, res, 'RolRecurso');
    }

    async function deleteItem(id) {
        debug('Eliminando rol_recurso');

        return Service.deleteItem(id, RolRecursoRepository, res, 'RolRecurso');
    }

    function validate(data) {
        Service.validate(data, {

            descricpcion: RolRecursoDescripcion,
            estado: RolRecursoEstado,
            persona: RolRecursoPersona,
            NavigationPreloadManager: RolRecursoNivel
        });
    }

    return {
        findAll,
        findById,
        createOrUpdate,
        deleteItem
    };
};