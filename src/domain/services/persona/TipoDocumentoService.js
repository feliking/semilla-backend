'use strict';

const debug = require('debug')('app:service:tipo_documento');
const Service = require('../Service');

module.exports = function tipoDocumentoService (repositories, valueObjects, res) {
  const { TipoDocumentoRepository } = repositories;
  const {
    TipoDocumentoDescripcion
  } = valueObjects;

  async function findAll (params = {}, rol, idTipoDocumento) {
    debug('Lista de tipo_documento|filtros');

    switch (rol) {
      case 'ADMIN':
        params.id_tipo_documento = idTipoDocumento;
        break;
      case 'USUARIO':
        params.id_tipo_documento = idTipoDocumento;
        break;
    }

    return Service.findAll(params, TipoDocumentoRepository, res, 'TipoDocumento');
  }

  async function findById (id) {
    debug('Buscando tipo_documento por ID', id);

    return Service.findById(id, TipoDocumentoRepository, res, 'TipoDocumento');
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar tipo_documento');

    validate(data);

    return Service.createOrUpdate(data, TipoDocumentoRepository, res, 'TipoDocumento');
  }

  async function deleteItem (id) {
    debug('Eliminando tipo_documento');

    return Service.deleteItem(id, TipoDocumentoRepository, res, 'TipoDocumento');
  }

  function validate (data) {
    Service.validate(data, {
      descripcion: TipoDocumentoDescripcion
    });
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
