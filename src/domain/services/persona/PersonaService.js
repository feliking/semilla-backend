'use strict';

const debug = require('debug')('app:service:tipo_documento');
const Service = require('../Service');

module.exports = function entidadService (repositories, valueObjects, res) {
  const { PersonaRepository } = repositories;

  async function findAll (params = {}, rol, idPersona) {
    debug('Lista de entidad|filtros');

    switch (rol) {
      case 'ADMIN':
        params.id_tipo_documento = idPersona;
        break;
      case 'USUARIO':
        params.id_tipo_documento = idPersona;
        break;
    }

    return Service.findAll(params, PersonaRepository, res, 'Persona');
  }

  async function findById (id) {
    debug('Buscando entidad por ID', id);

    return Service.findById(id, PersonaRepository, res, 'Persona');
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar entidad');

    return Service.createOrUpdate(data, PersonaRepository, res, 'Persona');
  }

  async function deleteItem (id) {
    debug('Eliminando entidad');

    return Service.deleteItem(id, PersonaRepository, res, 'Persona');
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
