'use strict';

const system = {
  sigla: process.env.SYS_SIGLA || 'RRHH'
};

module.exports = system;
