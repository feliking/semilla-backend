'use strict';

const debug = require('debug')('app:ldap');
require('dotenv').config();

const ldap = {
  ldapOpts: {
    url: process.env.LDAP_URL,
  },
  auth: process.env.LDAP_AUTH,
	adminDn: process.env.LDAP_ADMIN_DN,
  adminPassword: process.env.LDAP_ADMIN_PASSWORD,
  userSearchBase: process.env.LDAP_USER_SEARCH_BASE,
  usernameAttribute: process.env.LDAP_USER_NAME_ATTRIBUTE,
  logging: s => debug(s)
};

module.exports = ldap;
