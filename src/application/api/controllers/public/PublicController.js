'use strict';

const debug = require('debug')('app:controller:public');

module.exports = function setupPublicController (services) {
  const { UsuarioService } = services;

  async function obtenerUsuario (req, res, next) {
    let { usuario } = req.body;
    const result = await UsuarioService.obtenerUsuario(usuario)
    return res.send(result);
  }

  return {
    obtenerUsuario
  };
};
