'use strict';

module.exports = function setupPublic (api, controllers) {
  const { PublicController } = controllers;

  api.post('/usuario', PublicController.obtenerUsuario);

  return api;
};
