"use strict";

const debug = require("debug")("app:graphql");
const { ApolloServer } = require("apollo-server-express");
const { config } = require("../../common");

module.exports = async function setupGraphql(app, services, graphql) {
	debug("Iniciando servidor GraphQL");

	const rootQuery = `
      # Consultas Base
      type Query {
        ${graphql.queries.Query}
      }

      # Mutaciones Base
      type Mutation {
        ${graphql.queries.Mutation}
      }
    `;

	const server = new ApolloServer({
		typeDefs: [rootQuery].concat(graphql.schemes).join(""),
		resolvers: graphql.resolvers,
		formatError: (error) => {
			return {
				code: -1,
				data: error.name,
				message: error.message,
			};
		},
		playground: {
			endpoint: `http://localhost:3000/graphql`,
			settings: {
				"editor.theme": "dark",
			},
		},
	});

	server.applyMiddleware({ app, path: "/graphql" });

	// const schema = makeExecutableSchema({
	//   typeDefs: [rootQuery].concat(graphql.schemes),
	//   resolvers: graphql.resolvers
	// });
	// Creando endpoint de entrada de GraphQL
	// app.use('/graphql',
	//   graphqlExpress(req => ({
	//     schema,
	//     formatError: (error) => {
	//       return {
	//         code: -1,
	//         data: error.name,
	//         message: error.message
	//       };
	//     },
	//     context: req.context
	//   }))
	// );

	// Habilitando GraphiQL solo para desarrollo
	if (
		typeof process.env.NODE_ENV === "undefined" ||
		process.env.NODE_ENV !== "production"
	) {
		// app.use('/graphiql',
		//   graphiqlExpress({
		//     endpointURL: '/graphql'
		//   })
		// );
	}

	return app;
};