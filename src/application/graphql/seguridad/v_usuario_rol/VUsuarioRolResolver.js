"use strict";

module.exports = function setupResolver(services, res) {
  const { VUsuarioRolService } = services;

  return {
    Query: {
      v_usuario_rols: async (_, args, context) => {
        let items = await VUsuarioRolService.findAll(
          args,
          context.rol,
          context.id_v_usuario_rol
        );
        return res(items);
      },
      v_usuario_rol: async (_, args, context) => {
        let item = await VUsuarioRolService.findById(args.id);
        return res(item);
      },
    },
    Mutation: {
      v_usuario_rol_add: async (_, args, context) => {
        args.v_usuario_rol._v_usuario_rol_created = context.id_v_usuario_rol;
        let item = await VUsuarioRolService.createOrUpdate(args.v_usuario_rol);
        return res(item);
      },
      v_usuario_rol_edit: async (_, args, context) => {
        args.v_usuario_rol._v_usuario_rol_updated = context.id_v_usuario_rol;
        args.v_usuario_rol._updated_at = new Date();
        args.v_usuario_rol.id = args.id;
        let item = await VUsuarioRolService.createOrUpdate(args.v_usuario_rol);
        return res(item);
      },
      v_usuario_rol_delete: async (_, args, context) => {
        let deleted = await VUsuarioRolService.deleteItem(args.id);
        return { deleted: res(deleted) };
      },
    },
  };
};
