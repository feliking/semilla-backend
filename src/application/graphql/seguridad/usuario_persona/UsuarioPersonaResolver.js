'use strict';

module.exports = function setupResolver(services, res) {
    const { UsuarioPersonaService } = services;

    return {
        Query: {
            usuario_personas: async(_, args, context) => {

                let items = await UsuarioPersonaService.findAll(args, context.rol, context.id_usuario_persona);
                return res(items);
            },
            usuario_persona: async(_, args, context) => {

                let item = await UsuarioPersonaService.findById(args.id);
                return res(item);
            }
        },
        Mutation: {
            usuario_personaAdd: async(_, args, context) => {

                args.usuario_persona._user_created = context.id_usuario;
                let item = await UsuarioPersonaService.createOrUpdate(args.usuario_persona);
                return res(item);
            },
            usuario_personaEdit: async(_, args, context) => {

                args.usuario_persona._user_updated = context.id_usuario;
                args.usuario_persona._updated_at = new Date();
                args.usuario_persona.id = args.id;
                let item = await UsuarioPersonaService.createOrUpdate(args.usuario_persona);
                return res(item);
            },
            usuario_personaDelete: async(_, args, context) => {

                let deleted = await UsuarioPersonaService.deleteItem(args.id);
                return { deleted: res(deleted) };
            }
        }
    };
};