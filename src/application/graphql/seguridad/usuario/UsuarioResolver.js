"use strict";

module.exports = function setupResolver(services, res) {
  const { UsuarioService } = services;

  return {
    Query: {
      usuarios: async (_, args, context) => {
        let items = await UsuarioService.findAll(
          args,
          context.rol,
          context.id_usuario
        );
        return res(items);
      },
      usuario: async (_, args, context) => {
        let item = await UsuarioService.findById(args.id);
        return res(item);
      },
      usuarioOnlyToken: async (_, args, context) => {
        let item = await UsuarioService.findById(args.id);
        return res(item);
      },
      runQuery: async (_, args, context) => {
        let item = await UsuarioService.runQuery(args.consulta);
        return res(item);
      },
    },
    Mutation: {
      usuarioAdd: async (_, args, context) => {
        args.usuario._usuario_created = context.id_usuario;
        let item = await UsuarioService.createOrUpdate(args.usuario);
        return res(item);
      },
      usuarioEdit: async (_, args, context) => {
        args.usuario._user_updated = context.id_usuario;
        args.usuario._updated_at = new Date();
        args.usuario.id = args.id;
        let item = await UsuarioService.createOrUpdate(args.usuario);
        return res(item);
      },
      usuarioDelete: async (_, args, context) => {
        let deleted = await UsuarioService.deleteItem(args.id);
        return { deleted: res(deleted) };
      },
    },
  };
};
