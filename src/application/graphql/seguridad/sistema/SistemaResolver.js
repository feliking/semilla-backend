'use strict';

module.exports = function setupResolver(services, res) {
    const { SistemaService } = services;

    return {
        Query: {
            sistemas: async(_, args, context) => {

                let items = await SistemaService.findAll(args, context.rol, context.id_sistema);
                return res(items);
            },
            sistema: async(_, args, context) => {

                let item = await SistemaService.findById(args.id);
                return res(item);
            }
        },
        Mutation: {
            sistemaAdd: async(_, args, context) => {

                args.sistema._user_created = context.id_usuario;
                let item = await SistemaService.createOrUpdate(args.sistema);
                return res(item);
            },
            sistemaEdit: async(_, args, context) => {

                args.sistema._user_updated = context.id_usuario;
                args.sistema._updated_at = new Date();
                args.sistema.id = args.id;
                let item = await SistemaService.createOrUpdate(args.sistema);
                return res(item);
            },
            sistemaDelete: async(_, args, context) => {

                let deleted = await SistemaService.deleteItem(args.id);
                return { deleted: res(deleted) };
            }
        }
    };
};