'use strict';

module.exports = function setupResolver(services, res) {
    const { RolRecursoService } = services;

    return {
        Query: {
            rol_recursos: async(_, args, context) => {

                let items = await RolRecursoService.findAll(args, context.rol, context.id_rol_recurso);
                return res(items);
            },
            rol_recurso: async(_, args, context) => {

                let item = await RolRecursoService.findById(args.id);
                return res(item);
            }
        },
        Mutation: {
            rol_recursoAdd: async(_, args, context) => {

                args.rol_recurso._user_created = context.id_usuario;
                let item = await RolRecursoService.createOrUpdate(args.rol_recurso);
                return res(item);
            },
            rol_recursoEdit: async(_, args, context) => {

                args.rol_recurso._user_updated = context.id_usuario;
                args.rol_recurso._updated_at = new Date();
                args.rol_recurso.id = args.id;
                let item = await RolRecursoService.createOrUpdate(args.rol_recurso);
                return res(item);
            },
            rol_recursoDelete: async(_, args, context) => {

                let deleted = await RolRecursoService.deleteItem(args.id);
                return { deleted: res(deleted) };
            }
        }
    };
};