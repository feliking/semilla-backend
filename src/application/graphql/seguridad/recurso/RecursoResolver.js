"use strict";

module.exports = function setupResolver(services, res) {
  const { RecursoService } = services;

  return {
    Query: {
      recursos: async (_, args, context) => {
				args.id_sistema = context.id_sistema
        let items = await RecursoService.findAll(
          args,
          context.rol,
          context.id_recurso
        );
        return res(items);
      },
      recurso: async (_, args, context) => {
        let item = await RecursoService.findById(args.id);
        return res(item);
      },
    },
    Mutation: {
      recursoAdd: async (_, args, context) => {
        args.recurso._user_created = context.id_usuario;
        let item = await RecursoService.createOrUpdate(args.recurso, context.roles);
        return res(item);
      },
      recursoEdit: async (_, args, context) => {
        args.recurso._user_updated = context.id_usuario;
        args.recurso._updated_at = new Date();
        args.recurso.id = args.id;
        let item = await RecursoService.createOrUpdate(args.recurso, context.roles);
        return res(item);
      },
      recursoDelete: async (_, args, context) => {
        let deleted = await RecursoService.deleteItem(args.id);
        return { deleted: res(deleted) };
      },
    },
  };
};
