"use strict";

module.exports = function setupResolver(services, res) {
  const { RolService } = services;

  return {
    Query: {
      roles: async (_, args, context) => {
				args.id_sistema = context.id_sistema;
        let items = await RolService.findAll(args, context.rol, context.id_rol);
        return res(items);
      },
      rol: async (_, args, context) => {
        let item = await RolService.findById(args.id);
        return res(item);
      },
    },
    Mutation: {
      rolAdd: async (_, args, context) => {
        args.rol._user_created = context.id_usuario;
        args.rol.id_sistema = context.id_sistema;
        let item = await RolService.createOrUpdate(args.rol);
        return res(item);
      },
      rolEdit: async (_, args, context) => {
        args.rol._user_updated = context.id_usuario;
        args.rol._updated_at = new Date();
        args.rol.id = args.id;
        let item = await RolService.createOrUpdate(args.rol);
        return res(item);
      },
      rolDelete: async (_, args, context) => {
        let deleted = await RolService.deleteItem(args.id);
        return { deleted: res(deleted) };
      },
      rolAddRecursos: async (_, args, context) => {
        args._user_created = context.id_usuario;
        args._created_at = new Date();
        let respuesta = await RolService.agregarRecursos(args);
        return { respuesta: res(respuesta) };
      },
    },
  };
};
