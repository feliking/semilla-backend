'use strict';

module.exports = function setupResolver (services, res) {
  const { PersonaService } = services;

  return {
    Query: {
      personas: async (_, args, context) => {

        let items = await PersonaService.findAll(args, context.rol, context.id_persona);
        return res(items);
      },
      persona: async (_, args, context) => {

        let item = await PersonaService.findById(args.id);
        return res(item);
      }
    },
    Mutation: {
      personaAdd: async (_, args, context) => {

        args.persona._user_created = context.id_usuario;
        let item = await PersonaService.createOrUpdate(args.persona);
        return res(item);
      },
      personaEdit: async (_, args, context) => {

        args.persona._user_updated = context.id_usuario;
        args.persona._updated_at = new Date();
        args.persona.id = args.id;
        let item = await PersonaService.createOrUpdate(args.persona);
        return res(item);
      },
      personaDelete: async (_, args, context) => {

        let deleted = await PersonaService.deleteItem(args.id);
        return { deleted: res(deleted) };
      }
    }
  };
};
