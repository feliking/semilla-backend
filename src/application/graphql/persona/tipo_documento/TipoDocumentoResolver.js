'use strict';

module.exports = function setupResolver (services, res) {
  const { TipoDocumentoService } = services;

  return {
    Query: {
      tipoDocumentos: async (_, args, context) => {

        let items = await TipoDocumentoService.findAll(args, context.rol, context.id_tipo_documento);
        return res(items);
      },
      tipoDocumento: async (_, args, context) => {

        let item = await TipoDocumentoService.findById(args.id);
        return res(item);
      }
    },
    Mutation: {
      tipoDocumentoAdd: async (_, args, context) => {

        args.tipo_documento._user_created = context.id_usuario;
        let item = await TipoDocumentoService.createOrUpdate(args.tipo_documento);
        return res(item);
      },
      tipoDocumentoEdit: async (_, args, context) => {

        args.tipo_documento._user_updated = context.id_usuario;
        args.tipo_documento._updated_at = new Date();
        args.tipo_documento.id = args.id;
        let item = await TipoDocumentoService.createOrUpdate(args.tipo_documento);
        return res(item);
      },
      tipoDocumentoDelete: async (_, args, context) => {

        let deleted = await TipoDocumentoService.deleteItem(args.id);
        return { deleted: res(deleted) };
      }
    }
  };
};
