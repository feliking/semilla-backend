'use strict';

module.exports = function setupResolver (services, res) {
  const { EstadoService } = services;

  return {
    Query: {
      estados: async (_, args, context) => {

        let items = await EstadoService.findAll(args, context.rol, context.id_estado);
        return res(items);
      },
      estado: async (_, args, context) => {

        let item = await EstadoService.findById(args.id);
        return res(item);
      }
    },
    Mutation: {
      estadoAdd: async (_, args, context) => {

        args.estado._user_created = context.id_usuario;
        let item = await EstadoService.createOrUpdate(args.estado);
        return res(item);
      },
      estadoEdit: async (_, args, context) => {

        args.estado._user_updated = context.id_usuario;
        args.estado._updated_at = new Date();
        args.estado.id = args.id;
        let item = await EstadoService.createOrUpdate(args.estado);
        return res(item);
      },
      estadoDelete: async (_, args, context) => {

        let deleted = await EstadoService.deleteItem(args.id);
        return { deleted: res(deleted) };
      }
    }
  };
};
